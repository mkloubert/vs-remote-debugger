# Change Log (vs-remote-debugger)

## 2.0.0 (October 14th, 2017; multi root support)

* started to refactor to new, upcoming [Multi Root Workspace API](https://github.com/Microsoft/vscode/wiki/Extension-Authoring:-Adopting-Multi-Root-Workspace-APIs)

## 1.9.9 (November 13th, 2016)

* last stable release of [v1](https://github.com/mkloubert/vs-remote-debugger/tree/v1) branch

## 1.0.0 (November 1st, 2016)

* first stable release
